#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

int main()
{
	int fd;
	DatosMemCompartida *pmem_compartida;
	fd = open("mem_compartida", O_RDWR);
	
	if(fd<0)
	{
		perror("Error abriendo el fichero");
		return 1;
	}
	
	pmem_compartida = static_cast <DatosMemCompartida*> (mmap(0, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));
	if(pmem_compartida == MAP_FAILED)
	{
		perror("Error en la proyeccion en memoria");
		close(fd);
		return 1;
	}
	
	while(1)
	{
		if((pmem_compartida->esfera.centro.y) < ((pmem_compartida->raqueta1.y1+pmem_compartida->raqueta1.y2)/2)) //centro de raqueta1
		{
			pmem_compartida -> accion = -1;
		}
		if((pmem_compartida->esfera.centro.y) == ((pmem_compartida->raqueta1.y1+pmem_compartida->raqueta1.y2)/2)) //centro de raqueta1
		{
			pmem_compartida -> accion = 0;
		}
		if((pmem_compartida->esfera.centro.y) > ((pmem_compartida->raqueta1.y1+pmem_compartida->raqueta1.y2)/2)) //centro de raqueta1
		{
			pmem_compartida -> accion = 1;
		}
		usleep(25000);
	}
	munmap(pmem_compartida, sizeof(DatosMemCompartida));
	unlink("mem_compartida");
	return 0;
}
