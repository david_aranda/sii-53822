#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main()
{
int tuberia;
char mensaje[1000];

if(mkfifo("logger_tuberia", 0777)<0)
{
	perror("Error en la tuberia");
	return 1;
}

if((tuberia=open("logger_tuberia",O_RDONLY))<0)
{
	perror("Error abriendo la tuberia");
	return 1;
}

while(1)
{
	if((read(tuberia, mensaje, 1000))!=0)
	{
		printf("%s", mensaje);
	}
}

close(tuberia);
unlink("logger_tuberia");
}

