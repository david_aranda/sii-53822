#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	//close(tuberia);
	//unlink("logger_tuberia");
	munmap(pmem_compartida, sizeof(pmem_compartida));
	unlink("mem_compartida");
	close(fifo_s_c);
	unlink("FIFO_servidor_cliente");
	close(fifo_c_s);
	unlink("FIFO_cliente_servidor");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.VariarRadio();
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		//sprintf(mensaje, "El jugador 2 marca un punto, lleva un total de %d puntos\n", puntos2);
		//write(tuberia, mensaje, strlen(mensaje)+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		//sprintf(mensaje, "El jugador 1 marca un punto, lleva un total de %d puntos\n", puntos1);
		//write(tuberia, mensaje, strlen(mensaje)+1);
	}
	
	if(puntos1 == 3)
	{
		printf("Fin del Juego. Gana el jugador 1\n");
		exit(1);
	}	
	else if(puntos2 == 3)
	{
		printf("Fin del Juego. Gana el jugador 2\n");
		exit(1);
	}
	
	pmem_compartida -> esfera = esfera;
	pmem_compartida -> raqueta1 = jugador1;
	if(pmem_compartida -> accion == -1)
		OnKeyboardDown('s',0,0);
	else if (pmem_compartida -> accion == 0) {}
	else if (pmem_compartida -> accion == 1)
		OnKeyboardDown('w',0,0);
		
//fifo servidor a cliente
	read(fifo_s_c, cad, strlen(cad)+1);
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
	
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
/*	switch(key)
	{
	//case 'a':jugador1.velocidad.x=-1;break;
	//case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
*/	
	sprintf(cadena, "%c", key);
	write(fifo_c_s, cadena, strlen(cadena)+1);
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//tuberia
/*	if((tuberia=open("logger_tuberia", O_WRONLY))<0)
	{
		perror("No puede abrirse la tuberia");
		return;
	}
*/
//memoria compartida
	int memoria_mmap;
	memoria_mmap = open("mem_compartida", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (memoria_mmap < 0)
	{
		perror("Error creando el fichero");
		return;
	}
	mem_compartida.esfera = esfera;
	mem_compartida.raqueta1=jugador1;
	mem_compartida.accion = 0;
	if (write(memoria_mmap, &mem_compartida, sizeof(mem_compartida))<0)
	{
		perror("Error de escritura");
		return;
	}
	pmem_compartida = static_cast <DatosMemCompartida*>(mmap(NULL, sizeof(mem_compartida), PROT_READ|PROT_WRITE,MAP_SHARED, memoria_mmap, 0));
	if(pmem_compartida == MAP_FAILED)
	{
		perror("Error proyeccion en memoria");
		close(memoria_mmap);
		return;
	}
	close (memoria_mmap);

//fifo servidor a cliente
	if((mkfifo("FIFO_servidor_cliente", 0777))<0){
		perror("Error en la creacion de fifo_s_c");
		return;
	}
	if((fifo_s_c = open("FIFO_servidor_cliente", O_RDONLY))<0){
		perror("Errror abriendo el fifo_s_c");
		return;
	}
	
//fifo hilo
	if((mkfifo("FIFO_cliente_servidor", 0777))<0){
		perror("Error en la creacion de fifo_c_s");
		return;
	}
	if((fifo_c_s = open("FIFO_cliente_servidor", O_WRONLY))<0){
		perror("Error habriendo el fifo_c_s");
		return;
	}
}
