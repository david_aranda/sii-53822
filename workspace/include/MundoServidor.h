#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "Raqueta.h"
#include "Esfera.h"
#include <pthread.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "DatosMemCompartida.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	int tuberia;
	char mensaje[1000];
	
	//DatosMemCompartida mem_compartida;
	//DatosMemCompartida *pmem_compartida;
	
	int fifo_s_c;
	char cad[200];
	
	pthread_t thid;
	int fifo_c_s;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
