# Changelog
All notable changes to this project will be documented in this file.

## [v4.1] 16/12/2020
### Changed
- Se ha divido el juego en cliente, desde donde se controla el juego, y servidor, que dibuja el juego.

## [v3.1] 02/12/2020
### Added
- Se ha incluido un bot que controla uno de los jugadores y un contador de los puntos de cada jugador.

## [v2.2] 18/10/2020
### Added
- Se ha incluido el fichero README con las instrucciones del juego.

### Changed
- Se han corregido errores en el la implementación del movimiento de las plataformas y de la esfera.
- Se ha implementado que la esfera vaya reduciendo su radio con el paso del tiempo.

## [v1.3] 04/10/2020
### Changed
- Se ha modificado el codigo de Esfera.cpp y Plataforma.cpp para implementar el movimiento de estos.

## [v1.2] 16/10/2020
### Added
- Se ha incluido el fichero Changelog.

## [v1.1] 16/10/2020
### Added
- He añadido una linea de prueba en Esfera.cpp para ver el funcionamiento de bitbucket.









